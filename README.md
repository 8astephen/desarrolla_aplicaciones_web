**Desarrollo Aplicaciones Web con conexión a base de datos**
**Semestre:**
*5AVP*
**Alumno:**
*Stephen Manuel Camargo Ochoa*

- Practica #1 - 02/09/2022 - Práctica de ejemplo
Commit: db1146c422fc06a114da127bf64da0b11b750b14
Archivo: https://gitlab.com/8astephen/desarrolla_aplicaciones_web/-/blob/main/parcial1/practica_ejemplo.html

- Práctica #2 - 09/09/2022 - Práctica JavaScript
Commit: c6114774a0c5a58beb531d68a1be9b2235e170b8
Archivo: https://gitlab.com/8astephen/desarrolla_aplicaciones_web/-/blob/main/parcial1/practicaJavaScript.html

- Práctica #3 - 15/09/2022 - Práctica web con bases de datos - parte 1 
Commit: 93e8d247d71ffd33f1a11c081be25a26a71c4b86

- Práctica #4 - 19/09/2022 - Práctica Web con Bases de Datos - Vista de consulta de datos Commit: b46b0f11be210eaecc3b02f19047c8bb51caddf2

- Práctica #5 - 22/09/2022 - Práctica Web con Bases de Datos - Vista de registro de datos Commit: 6c398950290ba88d7354e835cd845e9759e44bc7

-Práctoca #6 - 26/09/2022 - Práctica Web con Bases de Datos - Conexión con base de datos Commit: 16f9b6c6b58b08933622d930127695cc319620b5
